
Readme
------

This is a simple filter module. It converts relative URLs to be
relative to their current location, not the Drupal home
directory. This eases the process of loading a directory of HTML
files into your Drupal database and mapping them to the same URLs they
had before Drupal.

For example, assume your Drupal is installed at
http://mysite.com/drupal/, and the page in question is available at
http://mysite.com/drupal/stories/index.html. Then the following
conversions will be done:

 href="http://example.com/" => href="http://example.com/"   (unchanged)
 href="/index.html"         => href="/index.html"          (unchanged)
 href="#anchor"             => href="stories/index.html#anchor"
 href="story-one.html"      => href="stories/story-one.html"
 src="http://example.com/example.gif"      (unchanged)
 src="/site-logo.gif"       => src="/site-logo.gif"        (unchanged)
 src="images/logo.gif"      => src="stories/images/logo.gif

The filter matches ' href="' or ' src="'. If your source doesn't have
a space before "href" or "src", or doesn't have a double-quote after
the equals sign, it won't do anything.

If you want a relative link to be unchanged, say ' hrefnot="' or '
srcnot="'. These will be changed to ' href="' and ' src="',
respectively, but will not be otherwise changed. This will make
relative links load relative to the Drupal directory, not the node's
directory.

 hrefnot="#anchor"         => href="#anchor"
 hrefnot="story-one.html"  => href="story-one.html"
 srcnot="images/logo.gif"  => src="images/logo.gif"

Note that including this filter in an input format will disable
caching for that input format.

Send comments to billstclair@gmail.com
